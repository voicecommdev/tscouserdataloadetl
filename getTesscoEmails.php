<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PhpOffice\PhpSpreadsheet\IOFactory;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addContactsFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    // CSV object
    $tesscoContacts = Reader::createFromPath('data/contacts.csv', 'r');
    $tesscoContacts->setDelimiter('|');
    $tesscoContacts->setHeaderOffset(0);
    $tesscoRecords = $tesscoContacts->getRecords();
    $progress = $cli->progress(count($tesscoContacts));
    foreach ($tesscoRecords as $row => $record)
    {
        $accountNumber = $record['Customer #'] ? $record['Customer #'] : '';
        $accountName = $record['Account Name'] ? $record['Account Name'] : '';
        if (isset($record['Email']) && !empty($record['Email'])) {
            $statement = $pdo->prepare("INSERT INTO ats_tessco_emails (email, account, account_name) VALUES (:email, :account, :account_name)");
            $statement->execute(['email' => $record['Email'], 'account' => $accountNumber, 'account_name' => $accountName]);
        }

        if (isset($record['TCOM 2.0 Username']) && !empty($record['TCOM 2.0 Username'])) {
            $statement = $pdo->prepare("INSERT INTO ats_tessco_emails (email, account, account_name) VALUES (:email, :account, :account_name)");
            $statement->execute(['email' => $record['Email'], 'account' => $accountNumber, 'account_name' => $accountName]);
        }
        $progress->advance();
    }

} catch (Exception $e) {
    print $e->getMessage();
}