<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PhpOffice\PhpSpreadsheet\IOFactory;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addContactsFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    // CSV object
//    $csv = Reader::createFromPath('data/TSCvsVC_account_data.csv', 'r');
//    $csv->setDelimiter(',');
//    $csv->setHeaderOffset(0);
//    $records = $csv->getRecords();
//    foreach ($records as $offset => $record)
//    {
//        var_dump($offset);
//        var_dump($record);
//    }

    $spreadsheet = IOFactory::load("data/TSCvsVC_account_data.xlsx");
    $sheet = $spreadsheet->getActiveSheet();
    $sheetArray = $sheet->toArray();
    $counter = 20;
    $progress = $cli->progress()->total(sizeof($sheetArray));
    foreach ($sheetArray as $row => $record)
    {
        if (isset($record[3]) && !isset($record[0])) {
            $statement = $pdo->prepare("INSERT INTO ats_tessco_vc_unique_accounts (tessco_account_number, vc_account_number) VALUES (:exclusion, :vcaccount)");
            $statement->execute(['exclusion' => NULL, 'vcaccount' => $record[3]]);
        }

        if (!isset($record[3]) && isset($record[0])) {
            $statement = $pdo->prepare("INSERT INTO ats_tessco_vc_unique_accounts (tessco_account_number, vc_account_number) VALUES (:exclusion, :vcaccount)");
            $statement->execute(['exclusion' => $record[0], 'vcaccount' => NULL]);
        }
        $progress->advance();
    }

} catch (Exception $e) {
    print $e->getMessage();
}