<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PhpOffice\PhpSpreadsheet\IOFactory;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addContactsFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    // CSV object
    $tesscoContacts = Reader::createFromPath('data/contacts.csv', 'r');
    $tesscoContacts->setDelimiter('|');
    $tesscoContacts->setHeaderOffset(0);
    $tesscoRecords = $tesscoContacts->getRecords();
    $progress = $cli->progress(count($tesscoContacts));
    foreach ($tesscoRecords as $row => $record)
    {
        $params = [
            'customer_id' => $record['Customer #'],
            'account_name' => $record['Account Name'],
            'salutation' => $record['Salutation'],
            'first_name' => $record['First Name'],
            'last_name' => $record['Last Name'],
            'title' => $record['Title'],
            'mailing_street' => $record['Mailing Street'],
            'mailing_city' => $record['Mailing City'],
            'mailing_state_province' => $record['Mailing State/Province'],
            'mailing_postal_code' => $record['Mailing Zip/Postal Code'],
            'mailing_country' => $record['Mailing Country'],
            'phone' => $record['Phone'],
            'fax' => $record['Fax'],
            'mobile' => $record['Mobile'],
            'email' => $record['Email'],
            'tcom_username' => $record['TCOM 2.0 Username'],
            'position_code' => $record['Position Code'],
            'contact_record_type' => $record['Contact Record Type']
        ];
        $statement = $pdo->prepare("INSERT INTO ats_customer_contact_extract 
                                                (customer_id, 
                                                account_name, 
                                                salutation,
                                                first_name,
                                                last_name,
                                                title,
                                                mailing_street,
                                                mailing_city,
                                                mailing_state_province,
                                                mailing_postal_code,
                                                mailing_country,
                                                phone,
                                                fax,
                                                mobile,
                                                email,
                                                tcom_username,
                                                position_code,
                                                contact_record_type)
                                                VALUES 
                                                (:customer_id, 
                                                :account_name, 
                                                :salutation,
                                                :first_name,
                                                :last_name,
                                                :title,
                                                :mailing_street,
                                                :mailing_city,
                                                :mailing_state_province,
                                                :mailing_postal_code,
                                                :mailing_country,
                                                :phone,
                                                :fax,
                                                :mobile,
                                                :email,
                                                :tcom_username,
                                                :position_code,
                                                :contact_record_type)");
        $statement->execute($params);

        $progress->advance();
    }

} catch (Exception $e) {
    print $e->getMessage();
}