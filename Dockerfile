FROM voicecomm/lamp-base:1.3.6

RUN mkdir /root/container_setup

COPY ./container_setup ./root/container_setup

RUN chown -R www-data:www-data /var/www/

RUN chmod -R 755 /root/container_setup/*
RUN chmod -R 755 /var/www/

RUN mv /root/container_setup/vhost.conf /etc/apache2/sites-available/vhost.conf
RUN mv /root/container_setup/addl-configs.conf /etc/apache2/conf-available/addl-configs.conf

RUN mkdir /var/log/etl

  #RUN composer install
RUN a2enmod rewrite headers
RUN a2ensite vhost.conf
RUN a2dissite 000-default.conf

EXPOSE 80
CMD apachectl -D FOREGROUND
