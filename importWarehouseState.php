<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addUsersFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    // CSV object
    $csv = Reader::createFromPath('data/warehouse_locations.csv', 'r');
    $csv->setDelimiter(',');
    $csv->setHeaderOffset(0);
    $records = $csv->getRecords();
    $progress = $cli->progress(sizeof($csv));
    foreach ($records as $offset => $record)
    {
        $progress->advance(1, $record['Location Code']);
        /*
         * Check if the record exists
         */
        $checkWarehouseLocationsStatement = $pdo->prepare("SELECT * FROM warehouse_locations WHERE location = :location");
        $checkWarehouseLocationsStatement->execute(['location' => $record['Location Code']]);
        $checkResult = $checkWarehouseLocationsStatement->fetchAll(PDO::FETCH_ASSOC);

        // if it does, update it.
        if (!empty($checkResult))
        {
            $updateLocationsStatement = $pdo->prepare("UPDATE warehouse_locations SET location_order = :location_order, master_sort_order = :master_sort_order WHERE location = :location AND location_type = :location_type");
            $updateLocationsStatement->execute([
                'location' => $record['Location Code'],
                'location_type' => $record['Location Type'],
                'location_order' => $record['Order'],
                'master_sort_order' => $record['Master Sort Order']
            ]);

            $updateItemsStatement = $pdo->prepare("UPDATE warehouse_location_items SET location_id = :location_id WHERE location_code = :location AND location_type = :location_type");
            $updateItemsStatement->execute([
                'location' => $record['Location Code'],
                'location_type' => $record['Location Type'],
                'location_id' => $checkResult[0]['loc_id']
            ]);
            continue;
        }

        // if it does not, insert it.
        $insertStatement = $pdo->prepare("INSERT INTO `myvoicecomm`.`warehouse_locations`
                                                    (`location`,
                                                    `location_type`,
                                                    `location_order`,
                                                    `master_sort_order`,
                                                    `data_entry_date`,
                                                    `data_entry_user`,
                                                    `data_entry_group`,
                                                    `data_entry_time`,
                                                    `data_created`,
                                                    `data_entry_domain`,
                                                    `shipping_id`,
                                                    `overstock_location`,
                                                    `secondary_location`,
                                                    `primary_location`,
                                                    `has_sibling_location`,
                                                    `has_parent_location`,
                                                    `replenishment_flag`,
                                                    `replenishment_flag_by`,
                                                    `replenishment_flag_part`,
                                                    `replenishment_assigned`,
                                                    `replenishment_already_flagged`,
                                                    `strikezone_location`,
                                                    `slowzone_location`,
                                                    `pick_location`,
                                                    `max_slot_volume`,
                                                    `cycle_count`,
                                                    `cycle_count_date`,
                                                    `cycle_count_user`)
                                                    VALUES
                                                    (:location,
                                                    :location_type,
                                                    :location_order,
                                                    :master_sort_order,
                                                    CURDATE(),
                                                    12504,
	                                                24,
                                                    CURTIME(),
                                                    CURDATE(),
                                                    1,
                                                    0,
                                                    :overstock_location,
                                                    '',
	                                                '',
                                                    '',
	                                                '',
                                                    NULL,
                                                    0,
                                                    0,
                                                    0,
                                                    false,
                                                    :strikezone_location,
                                                    '',
                                                    false,
                                                    :max_slot_volume,
                                                    0,
                                                    NULL,
                                                    0);");
        $insertStatement->execute([
            'location' => $record['Location Code'],
            'location_type' => $record['Location Type'],
            'location_order' => $record['Order'],
            'master_sort_order' => $record['Master Sort Order'],
            'max_slot_volume' => $record['Max Slot Volume'],
            'overstock_location' => $record['Overstock Location?'],
            'strikezone_location' => $record['Strike Zone']
        ]);

        $itemsInsertStatement = $pdo->prepare("INSERT INTO `myvoicecomm`.`warehouse_location_items`
		(
		`location_id`,
		`location_code`,
		`location_type`,
		`location_item_data`,
		`data_entry_date`,
		`data_entry_time`,
		`data_entry_group`,
		`data_entry_user`,
		`data_entry_domain`,
		`data_created`)
		VALUES
		(
		LAST_INSERT_ID(),
		:location,
		:location_type,
		'',
		CURDATE(),
		CURTIME(),
		24,
		12504,
		1,
		CURDATE());");

        $itemsInsertStatement->execute([
            'location' => $record['Location Code'],
            'location_type' => $record['Location Type']
        ]);
    }
} catch (Exception $e) {
    print $e->getMessage();
}