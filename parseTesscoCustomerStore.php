<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use League\Csv\Writer;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addStoresFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    // CSV object
    $tesscoContacts = Reader::createFromPath('data/stores.csv', 'r');
    $tesscoContacts->setDelimiter('|');
    $tesscoContacts->setHeaderOffset(0);
    $tesscoRecords = $tesscoContacts->getRecords();

    $progress = $cli->progress(count($tesscoContacts));
    $cli->green('Process beginning now.');
    foreach ($tesscoRecords as $index => $tesscoRecord)
    {
        $progress->advance(1, $tesscoRecord['Account Name']);
        $insertRecord = $pdo->prepare("INSERT INTO ats_tessco_shipping (type, customer_id, store_number, account_name, shipping_address_name, shipping_address_1, shipping_address_2, shipping_city, shipping_state, shipping_zip, shipping_country, shipping_province, customer_location_code, latest_invoice_date) 
                                                VALUES 
                                                       (:type, :customer_id, :store_number, :account_name, :shipping_address_name, :shipping_address_1, :shipping_address_2, :shipping_city, :shipping_state, :shipping_zip, :shipping_country, :shipping_province, :customer_location_code, :latest_invoice_date)");
        $insertRecord->execute([
            'type' => $tesscoRecord['Type'],
            'customer_id' => $tesscoRecord['Customer #'],
            'store_number' => $tesscoRecord['Store #'],
            'account_name' => $tesscoRecord['Account Name'],
            'shipping_address_name' => $tesscoRecord['Store Shipping Address Name'],
            'shipping_address_1' => $tesscoRecord['Store Shipping Address Line 1'],
            'shipping_address_2' => trim($tesscoRecord['Store Shipping Address Line 2'] . ' ' . $tesscoRecord['Store Shipping Address Line 3']),
            'shipping_city' => $tesscoRecord['Store Shipping City'],
            'shipping_state' => $tesscoRecord['Store Shipping State'],
            'shipping_zip' => $tesscoRecord['Store Shipping Zip'],
            'shipping_country' => $tesscoRecord['Store Shipping Country'],
            'shipping_province' => $tesscoRecord['Store Shipping Province'],
            'customer_location_code' => $tesscoRecord['Customer Location Code'],
            'latest_invoice_date' => $tesscoRecord['Latest invoice date'],
        ]);
    }

} catch (Exception $e) {
    print $e->getMessage();
}