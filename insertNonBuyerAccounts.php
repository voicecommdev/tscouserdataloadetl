<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PhpOffice\PhpSpreadsheet\IOFactory;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addContactsFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    $statement = $pdo->prepare("SELECT 
                                                *
                                            FROM
                                                myvoicecomm.ats_customer_contact_extract AS atscce
                                                    LEFT JOIN
                                                ats_tessco_to_vc_all_duplicate_accounts AS atsdupes ON atsdupes.tessco_account_number = atscce.customer_id
                                                WHERE atsdupes.vc_account_number IS NULL AND position_code LIKE '%Buyer%'
                                                GROUP BY customer_id;
                                            ");
    $statement->execute();
    $results = $statement->fetchAll();
    $progress = $cli->progress(count($results));
    foreach ($results as $index => $record)
    {
        $progress->advance(1, $record['account_name']);
        // Insert into the Users Table
        $insertUserStatement = $pdo->prepare("INSERT INTO ats_alt_users (
                                                         `user_fname`,
                                                         `user_lname`,
                                                         `user_phone`,
                                                         `login_name`,
                                                         `email`,
                                                         `title`,
                                                         `customer_name`)
                                                        VALUES (
                                                                :first_name,
                                                                :last_name,
                                                                :phone,
                                                                :login,
                                                                :email,
                                                                :salutation,
                                                                :account_name
                                                        )");
        $insertUserStatement->execute([
            'account_name' => $record['account_name'],
            'salutation' => $record['salutation'],
            'first_name' => $record['first_name'],
            'last_name' => $record['last_name'],
            'login' => empty($record['email']) ? $record['tcom_username'] : $record['email'],
            'email' => empty($record['email']) ? $record['tcom_username'] : $record['email'],
            'phone' => $record['phone']
        ]);
        // Insert into the Tessco Metadata Table
        $insertTesscoMetadataStatement = $pdo->prepare("INSERT INTO tessco_accounts (vc_user_id, tessco_account) VALUES (:user_id, :account_number)");
        $insertTesscoMetadataStatement->execute([
            'user_id' => $pdo->lastInsertId(),
            'account_number' => $record['customer_id']
        ]);
    }

    $ccListStatement = $pdo->prepare("UPDATE ats_alt_users INNER JOIN (SELECT 
                                                    u.user_id as user_id,
                                                    cce.customer_id,
                                                    GROUP_CONCAT(DISTINCT IF(cce.email = '', NULL, cce.email)
                                                        ORDER BY last_name
                                                        SEPARATOR ', ') AS cc_list_temp
                                                FROM
                                                    ats_alt_users AS u
                                                        LEFT JOIN
                                                    tessco_accounts AS t ON u.user_id = t.vc_user_id
                                                        LEFT JOIN
                                                    ats_customer_contact_extract AS cce ON tessco_account = cce.customer_id
                                                WHERE
                                                    tessco_account IS NOT NULL
                                                GROUP BY cce.customer_id) as cct ON ats_alt_users.user_id = cct.user_id
                                                SET ats_alt_users.cc_list = cct.cc_list_temp;");
    $ccListStatement->execute();

} catch (Exception $e) {
    print $e->getMessage();
}