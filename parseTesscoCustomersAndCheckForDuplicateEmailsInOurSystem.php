<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use League\Csv\Writer;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addStoresFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    // CSV object
    $tesscoContacts = Reader::createFromPath('data/contacts.csv', 'r');
    $tesscoContacts->setDelimiter('|');
    $tesscoContacts->setHeaderOffset(0);
    $tesscoRecords = $tesscoContacts->getRecords();

    $voicecommEmails = Reader::createFromPath('data/voicecommEmailsACTIVE.csv', 'r');
    $voicecommEmails->setHeaderOffset(0);
    $voicecommRecords = $voicecommEmails->getRecords();

    $writer = Writer::createFromPath('data/matchingEmailsReport.csv', 'w+');
    $recordsToWrite = [];
    $counter = 1;

    $progress = $cli->progress(count($voicecommEmails));
    $cli->blue('Process beginning now.');
    foreach ($voicecommRecords as $index => $voicecommRecord)
    {
        $voicecommEmail = strtolower($voicecommRecord['email']);
        $progress->advance(1, 'Iteration - ' . $counter . ' - Checking ' . $voicecommEmail);
        $progressTwo = $cli->progress(count($tesscoContacts));
        foreach ($tesscoRecords as $offset => $tesscoRecord)
        {
            $progressTwo->advance(1, 'Checking Customer: ' . $tesscoRecord['Customer #']);

            if (!isset($tesscoRecord['Email']) && !isset($tesscoRecord['TCOM 2.0 Username'])) {
//                $cli->red('No Tessco emails specified! Skipping Record: ' . $tesscoRecord['Customer #']);
                continue;
            }

            if (empty($tesscoRecord['Email']) && empty($tesscoRecord['TCOM 2.0 Username'])) {
//                $cli->red('Empty Tessco emails specified! Skipping Record: ' . $tesscoRecord['Customer #']);
                continue;
            }
            $tesscoEmailOne = strtolower($tesscoRecord['Email']);
            $tesscoEmailTwo = strtolower($tesscoRecord['TCOM 2.0 Username']);

            if ($voicecommEmail === $tesscoEmailOne)
            {
                array_push(
                    $recordsToWrite,
                    [
                        $voicecommRecord['user_id'],
                        $voicecommRecord['email'],
                        $tesscoRecord['Customer #'],
                        $tesscoRecord['Account Name'],
                        $tesscoRecord['Email']
                    ]
                );
            }

            if ($voicecommEmail === $tesscoEmailTwo)
            {
                array_push(
                    $recordsToWrite,
                    [
                        $voicecommRecord['user_id'],
                        $voicecommRecord['email'],
                        $tesscoRecord['Customer #'],
                        $tesscoRecord['Account Name'],
                        $tesscoRecord['TCOM 2.0 Username']
                    ]
                );
            }
        }
        $counter++;
    }
    $cli->blue('Duplicate Emails Count: ' . count($recordsToWrite));
    $cli->blue('Writing to file...');
    $writer->insertAll($recordsToWrite);
    $cli->blue('Report complete!');
} catch (Exception $e) {
    print $e->getMessage();
}