<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PhpOffice\PhpSpreadsheet\IOFactory;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addContactsFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    /*
     * We get all accounts that are not duplicates here:
     */
    $statement = $pdo->prepare("SELECT 
                                                *
                                            FROM
                                                ats_customer_contact_extract AS cce
                                                    LEFT JOIN
                                                ats_tessco_to_vc_all_duplicate_accounts AS dupes ON cce.customer_id = dupes.tessco_account_number
                                            WHERE
                                                dupes.vc_account_number IS NULL
                                            GROUP BY customer_id");
    $statement->execute();
    $results = $statement->fetchAll();
    $progress = $cli->progress(count($results));
    $cli->green('Total accounts to be inserted: ' . count($results));
    $counter = 0;
    foreach ($results as $index => $record)
    {
        $progress->advance(1, $record['account_name']);

        // Get relevant email information
        $accountEmailsStatement = $pdo->prepare("SELECT * FROM ats_customer_contact_extract WHERE customer_id = :customer_id");
        $accountEmailsStatement->execute(['customer_id' => $record['customer_id']]);
        $accountContacts = $accountEmailsStatement->fetchAll();
        $mainEmail = '';
        $ccList = [];
        foreach ($accountContacts as $contactRow => $contact)
        {
            // check for an email
            if (isset($contact['email']) && !empty($contact['email']) && filter_var($contact['email'], FILTER_VALIDATE_EMAIL))
            {
                array_push($ccList, $contact['email']);
                if ( strpos($contact['position_code'], 'Buyer') !== false && $mainEmail === '')
                {
                    $mainEmail = $contact['email'];
                }
            }

            // check for an email
            if (isset($contact['tcom_username']) && !empty($contact['tcom_username']) && filter_var($contact['tcom_username'], FILTER_VALIDATE_EMAIL))
            {
                array_push($ccList, $contact['tcom_username']);
                if ( strpos($contact['position_code'], 'Buyer') !== false && $mainEmail === '')
                {
                    $mainEmail = $contact['tcom_username'];
                }
            }
        }

        // default the main email to the first entry in the cc list if one hasn't been found yet.
        if ($mainEmail === '' && sizeof($ccList) > 0)
        {
            $mainEmail = $ccList[0];
        }

        // check for duplicates of the main email in the cc list.
        $listOfMainEmailOccurrencesInCCList = array_keys($ccList, $mainEmail);
        foreach ($listOfMainEmailOccurrencesInCCList as $listRow => $duplicateEmailIndex)
        {
            unset($ccList[$duplicateEmailIndex]);
        }

        // normalize keys after filtering out main email.
        $ccList = array_values($ccList);
        // Remove Duplicate Emails
        $ccList = array_unique($ccList);

        // Insert into the Users Table
        $insertUserStatement = $pdo->prepare("INSERT INTO users (
                                                         `user_fname`,
                                                         `user_lname`,
                                                         `user_phone`,
                                                         `login_name`,
                                                         `email`,
                                                         `title`,
                                                         `customer_name`,
                                                         `cc_list`,
                                                         `view_pricing`,
                                                         `quick_ship_enabled`,
                                                         `allow_password_reset`,
                                                         `password_days_term`,
                                                         `registration_date`,
                                                         `terms`,
                                                         `primary_domain`,
                                                         `pricing_level`,
                                                         `rep_id`,
                                                         `date_rep_assigned`,
                                                         `password`,
                                                         `group_id`,
                                                         `tessco_account`
                                                         )
                                                        VALUES (
                                                                :first_name,
                                                                :last_name,
                                                                :phone,
                                                                :login,
                                                                :email,
                                                                :salutation,
                                                                :account_name,
                                                                :cc_list,
                                                                'yes',
                                                                'yes',
                                                                'yes',
                                                                6000,
                                                                CURRENT_DATE(),
                                                                :terms,
                                                                1,
                                                                182,
                                                                6,
                                                                '2020-10-27',
                                                                'e6f74486cc59e92d23238b0a2da72a5dd05e21b5',
                                                                11,
                                                                :tessco_account
                                                        )");

        $badCharacters = ['"', "'", "`"];

        $insertUserStatement->execute([
            'account_name' => str_replace($badCharacters, '', $record['account_name']),
            'salutation' => str_replace($badCharacters, '', $record['salutation']),
            'first_name' => str_replace($badCharacters, '', $record['first_name']),
            'last_name' => str_replace($badCharacters, '', $record['last_name']),
            'login' => str_replace($badCharacters, '', $mainEmail),
            'email' => str_replace($badCharacters, '', $mainEmail),
            'phone' => str_replace($badCharacters, '', $record['phone']),
            'cc_list' => str_replace($badCharacters, '', implode(', ', $ccList)),
            'terms' => 'Credit Card',
            'tessco_account' => $record['customer_id']
        ]);
        $counter++;
    }
    $cli->green('Total accounts inserted: ' . $counter);

} catch (Exception $e) {
    print $e->getMessage();
}