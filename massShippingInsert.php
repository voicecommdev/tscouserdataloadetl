<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PhpOffice\PhpSpreadsheet\IOFactory;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addContactsFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    /*
     * We get all accounts that are not duplicates here:
     */
    $statement = $pdo->prepare("SELECT 
                                                u.tessco_account,
                                                u.user_id,
                                                shipping.*
                                            FROM
                                                users as u
                                                    INNER JOIN
                                                ats_tessco_shipping as shipping ON u.tessco_account = shipping.customer_id");
    $statement->execute();
    $results = $statement->fetchAll();
    $progress = $cli->progress(count($results));
    $cli->green('Total accounts to be inserted: ' . count($results));
    $counter = 0;
    foreach ($results as $index => $record)
    {
        $date = new DateTime('now');
        $firstName = '';
        $lastName = '';
        if (strpos($record['shipping_address_name'], ' ')) {
            $firstName = substr($record['shipping_address_name'],0, strrpos($record['shipping_address_name'],' '));
            $lastName = substr($record['shipping_address_name'], (strrpos($record['shipping_address_name'],' ') + 1));
        } else {
            $firstName = $record['shipping_address_name'];
            $lastName = $record['shipping_address_name'];
        }

        $zipCode = $record['shipping_zip'];
        if (strlen($zipCode) > 5) {
            $ending = substr($zipCode, 5);
            if ($ending === '-0000' || strlen($ending) < 5)
            {
                $zipCode = substr($zipCode, 0, 5);
            }
        }
        $progress->advance(1, $record['account_name']);
        $insertStatement = $pdo->prepare("INSERT INTO `myvoicecomm`.`user_contact`
                                                    (
                                                     `contact_employer`,
                                                     `contact_address`,
                                                     `contact_address_2`,
                                                     `contact_city`,
                                                     `contact_state`,
                                                     `contact_zip`,
                                                     `contact_country`,
                                                     `user_id`,
                                                     `contact_relationship`,
                                                     `customer_location_code`,
                                                     `data_entry_date`,
                                                     `data_entry_time`,
                                                     `data_created`,
                                                     `data_entry_domain`,
                                                     `iqm_store_id`,
                                                     `time_in_transit`,
                                                     `time_in_transit_fedex`,
                                                     `show_at_top`,
                                                     `default_credit_card`,
                                                     `data_entry_user`,
                                                     `data_entry_group`, `start_date`, `contact_fname`, `contact_lname`)
                                                    VALUES (:shipping_address_name, :shipping_address_1, :shipping_address_2, :shipping_city, :shipping_state, :shipping_zip, :shipping_country, :user_id, :contact_relationship, :customer_location_code, :data_created, :data_created_time, :data_created_date, 1, 0, 0, 0, 0, :credit_card, :data_entry_user, :data_entry_group, '2020-10-27', :first_name, :last_name)");

        $badCharacters = ['"', "'", "`"];
        $insertStatement->execute([
            'shipping_zip' => str_replace($badCharacters, '', $zipCode),
            'shipping_country' => $record['shipping_country'] === 'Canada' ? 41 : 236,
            'shipping_state' => $record['shipping_country'] === 'Canada' ? str_replace($badCharacters, '', $record['shipping_province']) : str_replace($badCharacters, '', $record['shipping_state']),
            'shipping_city' => str_replace($badCharacters, '', $record['shipping_city']),
            'shipping_address_1' => str_replace($badCharacters, '', $record['shipping_address_1']),
            'shipping_address_2' => str_replace($badCharacters, '', $record['shipping_address_2']),
            'shipping_address_name' => str_replace($badCharacters, '', $record['shipping_address_name']),
            'user_id' => str_replace($badCharacters, '', $record['user_id']),
            'contact_relationship' => "Shipping",
            'credit_card' => '',
            'data_created' => $date->format("Y-m-d"),
            'data_created_time' => $date->format("H:i:s"),
            'data_created_date' => $date->format("Y-m-d"),
            'customer_location_code' => $record['customer_location_code'] !== '0000' ? str_replace($badCharacters, '', $record['customer_location_code']) : '',
            'data_entry_user' => 12504,
            'data_entry_group' => 24,
            'first_name' => str_replace($badCharacters, '', $firstName),
            'last_name' => str_replace($badCharacters, '', $lastName)
        ]);
        $counter++;
    }
    $cli->green('Total accounts inserted: ' . $counter);

} catch (Exception $e) {
    print $e->getMessage();
}