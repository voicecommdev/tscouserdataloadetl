<?php
require_once "vendor/autoload.php";

use League\CLImate\CLImate;
use League\Csv\Reader;
use League\Csv\Writer;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

try {
    // create a log channel
    $log = new Logger('ETLLogger');
    $log->pushHandler(new StreamHandler('/var/log/etl/addStoresFromFileError.log', Logger::WARNING));

    // Pretty CLI interface.
    $cli = new CLImate();

    // Environment variables
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    $hostname = $_SERVER['VC_DB_HOST'];
    $database = $_SERVER['VC_DB_NAME'];
    $port = $_SERVER['VC_DB_PORT'];
    $username = $_SERVER['BACKEND_DB_USER'];
    $password = $_SERVER['BACKEND_DB_PASS'];

    // Database connection
    $pdo = new PDO("mysql:host=" . $hostname . ";port=" . $port . ";dbname=" . $database, $username, $password);

    // CSV object
    $tesscoContacts = Reader::createFromPath('data/accounts.csv', 'r');
    $tesscoContacts->setDelimiter('|');
    $tesscoContacts->setHeaderOffset(0);
    $tesscoRecords = $tesscoContacts->getRecords();

    $progress = $cli->progress(count($tesscoContacts));
    $cli->green('Process beginning now.');
    foreach ($tesscoRecords as $index => $tesscoRecord)
    {
        $progress->advance(1, $tesscoRecord['Account Name']);
        $insertRecord = $pdo->prepare("INSERT INTO ats_tessco_billing (type, customer_id, account_name, billing_address_name, billing_address_1, billing_address_2, billing_city, billing_state, billing_zip, billing_country, billing_province, payment_terms, website, market_code) 
                                                VALUES 
                                                       (:type, :customer_id, :account_name, :billing_address_name, :billing_address_1, :billing_address_2, :billing_city, :billing_state, :billing_zip, :billing_country, :billing_province, :payment_terms, :website, :market_code)");
        $insertRecord->execute([
            'type' => $tesscoRecord['Type'],
            'customer_id' => $tesscoRecord['Customer #'],
            'account_name' => $tesscoRecord['Account Name'],
            'billing_address_name' => $tesscoRecord['Billing Address Name^'],
            'billing_address_1' => $tesscoRecord['Billing Address Line 1^'],
            'billing_address_2' => trim($tesscoRecord['Billing Address Line 2'] . ' ' . $tesscoRecord['Billing Address Line 3']),
            'billing_city' => $tesscoRecord['Billing City'],
            'billing_state' => $tesscoRecord['Billing State'],
            'billing_zip' => $tesscoRecord['Billing Zip^'],
            'billing_country' => $tesscoRecord['Billing Country^'],
            'billing_province' => $tesscoRecord['Billing Province'],
            'payment_terms' => $tesscoRecord['Payment Terms'],
            'website' => $tesscoRecord['Website'],
            'market_code' => $tesscoRecord['Market Code^'],
        ]);
    }

} catch (Exception $e) {
    print $e->getMessage();
}